gdjs.StartingCode = {};
gdjs.StartingCode.GDfsnObjects1= [];
gdjs.StartingCode.GDfsnObjects2= [];
gdjs.StartingCode.GDfsnObjects3= [];
gdjs.StartingCode.GDxxObjects1= [];
gdjs.StartingCode.GDxxObjects2= [];
gdjs.StartingCode.GDxxObjects3= [];
gdjs.StartingCode.GDbox2Objects1= [];
gdjs.StartingCode.GDbox2Objects2= [];
gdjs.StartingCode.GDbox2Objects3= [];
gdjs.StartingCode.GDbox1Objects1= [];
gdjs.StartingCode.GDbox1Objects2= [];
gdjs.StartingCode.GDbox1Objects3= [];
gdjs.StartingCode.GDcoverObjects1= [];
gdjs.StartingCode.GDcoverObjects2= [];
gdjs.StartingCode.GDcoverObjects3= [];
gdjs.StartingCode.GDcrowdfundingObjects1= [];
gdjs.StartingCode.GDcrowdfundingObjects2= [];
gdjs.StartingCode.GDcrowdfundingObjects3= [];
gdjs.StartingCode.GDcommentsObjects1= [];
gdjs.StartingCode.GDcommentsObjects2= [];
gdjs.StartingCode.GDcommentsObjects3= [];
gdjs.StartingCode.GDmissionObjects1= [];
gdjs.StartingCode.GDmissionObjects2= [];
gdjs.StartingCode.GDmissionObjects3= [];
gdjs.StartingCode.GDstartObjects1= [];
gdjs.StartingCode.GDstartObjects2= [];
gdjs.StartingCode.GDstartObjects3= [];
gdjs.StartingCode.GDDisObjects1= [];
gdjs.StartingCode.GDDisObjects2= [];
gdjs.StartingCode.GDDisObjects3= [];
gdjs.StartingCode.GDboxObjects1= [];
gdjs.StartingCode.GDboxObjects2= [];
gdjs.StartingCode.GDboxObjects3= [];
gdjs.StartingCode.GDrecipientObjects1= [];
gdjs.StartingCode.GDrecipientObjects2= [];
gdjs.StartingCode.GDrecipientObjects3= [];
gdjs.StartingCode.GDcancelObjects1= [];
gdjs.StartingCode.GDcancelObjects2= [];
gdjs.StartingCode.GDcancelObjects3= [];
gdjs.StartingCode.GDw1Objects1= [];
gdjs.StartingCode.GDw1Objects2= [];
gdjs.StartingCode.GDw1Objects3= [];
gdjs.StartingCode.GDxObjects1= [];
gdjs.StartingCode.GDxObjects2= [];
gdjs.StartingCode.GDxObjects3= [];
gdjs.StartingCode.GDNewObjectObjects1= [];
gdjs.StartingCode.GDNewObjectObjects2= [];
gdjs.StartingCode.GDNewObjectObjects3= [];

gdjs.StartingCode.conditionTrue_0 = {val:false};
gdjs.StartingCode.condition0IsTrue_0 = {val:false};
gdjs.StartingCode.condition1IsTrue_0 = {val:false};
gdjs.StartingCode.condition2IsTrue_0 = {val:false};
gdjs.StartingCode.conditionTrue_1 = {val:false};
gdjs.StartingCode.condition0IsTrue_1 = {val:false};
gdjs.StartingCode.condition1IsTrue_1 = {val:false};
gdjs.StartingCode.condition2IsTrue_1 = {val:false};


gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDmissionObjects2Objects = Hashtable.newFrom({"mission": gdjs.StartingCode.GDmissionObjects2});gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDcrowdfundingObjects2Objects = Hashtable.newFrom({"crowdfunding": gdjs.StartingCode.GDcrowdfundingObjects2});gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDstartObjects2Objects = Hashtable.newFrom({"start": gdjs.StartingCode.GDstartObjects2});gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDcommentsObjects2Objects = Hashtable.newFrom({"comments": gdjs.StartingCode.GDcommentsObjects2});gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDcancelObjects2Objects = Hashtable.newFrom({"cancel": gdjs.StartingCode.GDcancelObjects2});gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDw1Objects1Objects = Hashtable.newFrom({"w1": gdjs.StartingCode.GDw1Objects1});gdjs.StartingCode.eventsList0 = function(runtimeScene) {

{

gdjs.StartingCode.GDmissionObjects2.createFrom(runtimeScene.getObjects("mission"));

gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDmissionObjects2Objects, runtimeScene, true, false);
}if (gdjs.StartingCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("m").setNumber(1);
}}

}


{

gdjs.StartingCode.GDcrowdfundingObjects2.createFrom(runtimeScene.getObjects("crowdfunding"));

gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDcrowdfundingObjects2Objects, runtimeScene, true, false);
}if (gdjs.StartingCode.condition0IsTrue_0.val) {
}

}


{

gdjs.StartingCode.GDstartObjects2.createFrom(runtimeScene.getObjects("start"));

gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDstartObjects2Objects, runtimeScene, true, false);
}if (gdjs.StartingCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("m").setNumber(2);
}}

}


{

gdjs.StartingCode.GDcommentsObjects2.createFrom(runtimeScene.getObjects("comments"));

gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDcommentsObjects2Objects, runtimeScene, true, false);
}if (gdjs.StartingCode.condition0IsTrue_0.val) {
{gdjs.evtTools.window.openURL("http://netorare.crypto/comments.html", runtimeScene);
}}

}


{

gdjs.StartingCode.GDcancelObjects2.createFrom(runtimeScene.getObjects("cancel"));

gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDcancelObjects2Objects, runtimeScene, true, false);
}if (gdjs.StartingCode.condition0IsTrue_0.val) {
{runtimeScene.getVariables().get("m").setNumber(0);
}}

}


{

gdjs.StartingCode.GDw1Objects1.createFrom(runtimeScene.getObjects("w1"));

gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDw1Objects1Objects, runtimeScene, true, false);
}if (gdjs.StartingCode.condition0IsTrue_0.val) {
{gdjs.evtTools.window.openURL("http://netorare.crypto/crowdfunding.html", runtimeScene);
}}

}


};gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDfsnObjects1Objects = Hashtable.newFrom({"fsn": gdjs.StartingCode.GDfsnObjects1});gdjs.StartingCode.eventsList1 = function(runtimeScene) {

{


{
{gdjs.evtTools.window.setFullScreen(runtimeScene, false, true);
}}

}


{


{
{gdjs.evtTools.window.setFullScreen(runtimeScene, true, true);
}}

}


};gdjs.StartingCode.eventsList2 = function(runtimeScene) {

{


gdjs.StartingCode.condition0IsTrue_0.val = false;
gdjs.StartingCode.condition1IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.StartingCode.condition0IsTrue_0.val ) {
{
{gdjs.StartingCode.conditionTrue_1 = gdjs.StartingCode.condition1IsTrue_0;
gdjs.StartingCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7582684);
}
}}
if (gdjs.StartingCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.StartingCode.eventsList1(runtimeScene);} //End of subevents
}

}


};gdjs.StartingCode.eventsList3 = function(runtimeScene) {

{


gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if (gdjs.StartingCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.StartingCode.eventsList0(runtimeScene);} //End of subevents
}

}


{


gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("m")) == 0;
}if (gdjs.StartingCode.condition0IsTrue_0.val) {
gdjs.StartingCode.GDcoverObjects1.createFrom(runtimeScene.getObjects("cover"));
{gdjs.evtTools.camera.centerCamera(runtimeScene, (gdjs.StartingCode.GDcoverObjects1.length !== 0 ? gdjs.StartingCode.GDcoverObjects1[0] : null), true, "", 0);
}}

}


{


gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("m")) == 1;
}if (gdjs.StartingCode.condition0IsTrue_0.val) {
gdjs.StartingCode.GDbox1Objects1.createFrom(runtimeScene.getObjects("box1"));
{gdjs.evtTools.camera.centerCamera(runtimeScene, (gdjs.StartingCode.GDbox1Objects1.length !== 0 ? gdjs.StartingCode.GDbox1Objects1[0] : null), true, "", 0);
}}

}


{


gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.common.getVariableNumber(runtimeScene.getVariables().get("m")) == 2;
}if (gdjs.StartingCode.condition0IsTrue_0.val) {
gdjs.StartingCode.GDbox2Objects1.createFrom(runtimeScene.getObjects("box2"));
{gdjs.evtTools.camera.centerCamera(runtimeScene, (gdjs.StartingCode.GDbox2Objects1.length !== 0 ? gdjs.StartingCode.GDbox2Objects1[0] : null), true, "", 0);
}}

}


{



}


{

gdjs.StartingCode.GDfsnObjects1.createFrom(runtimeScene.getObjects("fsn"));

gdjs.StartingCode.condition0IsTrue_0.val = false;
{
gdjs.StartingCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.StartingCode.mapOfGDgdjs_46StartingCode_46GDfsnObjects1Objects, runtimeScene, true, false);
}if (gdjs.StartingCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.StartingCode.eventsList2(runtimeScene);} //End of subevents
}

}


};

gdjs.StartingCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.StartingCode.GDfsnObjects1.length = 0;
gdjs.StartingCode.GDfsnObjects2.length = 0;
gdjs.StartingCode.GDfsnObjects3.length = 0;
gdjs.StartingCode.GDxxObjects1.length = 0;
gdjs.StartingCode.GDxxObjects2.length = 0;
gdjs.StartingCode.GDxxObjects3.length = 0;
gdjs.StartingCode.GDbox2Objects1.length = 0;
gdjs.StartingCode.GDbox2Objects2.length = 0;
gdjs.StartingCode.GDbox2Objects3.length = 0;
gdjs.StartingCode.GDbox1Objects1.length = 0;
gdjs.StartingCode.GDbox1Objects2.length = 0;
gdjs.StartingCode.GDbox1Objects3.length = 0;
gdjs.StartingCode.GDcoverObjects1.length = 0;
gdjs.StartingCode.GDcoverObjects2.length = 0;
gdjs.StartingCode.GDcoverObjects3.length = 0;
gdjs.StartingCode.GDcrowdfundingObjects1.length = 0;
gdjs.StartingCode.GDcrowdfundingObjects2.length = 0;
gdjs.StartingCode.GDcrowdfundingObjects3.length = 0;
gdjs.StartingCode.GDcommentsObjects1.length = 0;
gdjs.StartingCode.GDcommentsObjects2.length = 0;
gdjs.StartingCode.GDcommentsObjects3.length = 0;
gdjs.StartingCode.GDmissionObjects1.length = 0;
gdjs.StartingCode.GDmissionObjects2.length = 0;
gdjs.StartingCode.GDmissionObjects3.length = 0;
gdjs.StartingCode.GDstartObjects1.length = 0;
gdjs.StartingCode.GDstartObjects2.length = 0;
gdjs.StartingCode.GDstartObjects3.length = 0;
gdjs.StartingCode.GDDisObjects1.length = 0;
gdjs.StartingCode.GDDisObjects2.length = 0;
gdjs.StartingCode.GDDisObjects3.length = 0;
gdjs.StartingCode.GDboxObjects1.length = 0;
gdjs.StartingCode.GDboxObjects2.length = 0;
gdjs.StartingCode.GDboxObjects3.length = 0;
gdjs.StartingCode.GDrecipientObjects1.length = 0;
gdjs.StartingCode.GDrecipientObjects2.length = 0;
gdjs.StartingCode.GDrecipientObjects3.length = 0;
gdjs.StartingCode.GDcancelObjects1.length = 0;
gdjs.StartingCode.GDcancelObjects2.length = 0;
gdjs.StartingCode.GDcancelObjects3.length = 0;
gdjs.StartingCode.GDw1Objects1.length = 0;
gdjs.StartingCode.GDw1Objects2.length = 0;
gdjs.StartingCode.GDw1Objects3.length = 0;
gdjs.StartingCode.GDxObjects1.length = 0;
gdjs.StartingCode.GDxObjects2.length = 0;
gdjs.StartingCode.GDxObjects3.length = 0;
gdjs.StartingCode.GDNewObjectObjects1.length = 0;
gdjs.StartingCode.GDNewObjectObjects2.length = 0;
gdjs.StartingCode.GDNewObjectObjects3.length = 0;

gdjs.StartingCode.eventsList3(runtimeScene);
return;

}

gdjs['StartingCode'] = gdjs.StartingCode;
