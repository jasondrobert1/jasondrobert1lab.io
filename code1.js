gdjs.groundCode = {};
gdjs.groundCode.GDfsnObjects1= [];
gdjs.groundCode.GDfsnObjects2= [];
gdjs.groundCode.GDfsnObjects3= [];
gdjs.groundCode.GDplayerObjects1= [];
gdjs.groundCode.GDplayerObjects2= [];
gdjs.groundCode.GDplayerObjects3= [];
gdjs.groundCode.GDDetailsObjects1= [];
gdjs.groundCode.GDDetailsObjects2= [];
gdjs.groundCode.GDDetailsObjects3= [];
gdjs.groundCode.GDrh1Objects1= [];
gdjs.groundCode.GDrh1Objects2= [];
gdjs.groundCode.GDrh1Objects3= [];
gdjs.groundCode.GDclothesObjects1= [];
gdjs.groundCode.GDclothesObjects2= [];
gdjs.groundCode.GDclothesObjects3= [];
gdjs.groundCode.GDsg1Objects1= [];
gdjs.groundCode.GDsg1Objects2= [];
gdjs.groundCode.GDsg1Objects3= [];
gdjs.groundCode.GDhh1Objects1= [];
gdjs.groundCode.GDhh1Objects2= [];
gdjs.groundCode.GDhh1Objects3= [];
gdjs.groundCode.GDwbotObjects1= [];
gdjs.groundCode.GDwbotObjects2= [];
gdjs.groundCode.GDwbotObjects3= [];
gdjs.groundCode.GDwtopObjects1= [];
gdjs.groundCode.GDwtopObjects2= [];
gdjs.groundCode.GDwtopObjects3= [];
gdjs.groundCode.GDwrightObjects1= [];
gdjs.groundCode.GDwrightObjects2= [];
gdjs.groundCode.GDwrightObjects3= [];
gdjs.groundCode.GDwleftObjects1= [];
gdjs.groundCode.GDwleftObjects2= [];
gdjs.groundCode.GDwleftObjects3= [];

gdjs.groundCode.conditionTrue_0 = {val:false};
gdjs.groundCode.condition0IsTrue_0 = {val:false};
gdjs.groundCode.condition1IsTrue_0 = {val:false};
gdjs.groundCode.condition2IsTrue_0 = {val:false};
gdjs.groundCode.conditionTrue_1 = {val:false};
gdjs.groundCode.condition0IsTrue_1 = {val:false};
gdjs.groundCode.condition1IsTrue_1 = {val:false};
gdjs.groundCode.condition2IsTrue_1 = {val:false};


gdjs.groundCode.mapOfGDgdjs_46groundCode_46GDfsnObjects1Objects = Hashtable.newFrom({"fsn": gdjs.groundCode.GDfsnObjects1});gdjs.groundCode.eventsList0 = function(runtimeScene) {

{


{
{gdjs.evtTools.window.setFullScreen(runtimeScene, false, true);
}}

}


{


{
{gdjs.evtTools.window.setFullScreen(runtimeScene, true, true);
}}

}


};gdjs.groundCode.eventsList1 = function(runtimeScene) {

{


gdjs.groundCode.condition0IsTrue_0.val = false;
gdjs.groundCode.condition1IsTrue_0.val = false;
{
gdjs.groundCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonReleased(runtimeScene, "Left");
}if ( gdjs.groundCode.condition0IsTrue_0.val ) {
{
{gdjs.groundCode.conditionTrue_1 = gdjs.groundCode.condition1IsTrue_0;
gdjs.groundCode.conditionTrue_1.val = runtimeScene.getOnceTriggers().triggerOnce(7582684);
}
}}
if (gdjs.groundCode.condition1IsTrue_0.val) {

{ //Subevents
gdjs.groundCode.eventsList0(runtimeScene);} //End of subevents
}

}


};gdjs.groundCode.mapOfGDgdjs_46groundCode_46GDplayerObjects1Objects = Hashtable.newFrom({"player": gdjs.groundCode.GDplayerObjects1});gdjs.groundCode.mapOfGDgdjs_46groundCode_46GDclothesObjects1Objects = Hashtable.newFrom({"clothes": gdjs.groundCode.GDclothesObjects1});gdjs.groundCode.mapOfGDgdjs_46groundCode_46GDwbotObjects1Objects = Hashtable.newFrom({"wbot": gdjs.groundCode.GDwbotObjects1});gdjs.groundCode.eventsList2 = function(runtimeScene) {

{

gdjs.groundCode.GDwbotObjects1.createFrom(runtimeScene.getObjects("wbot"));

gdjs.groundCode.condition0IsTrue_0.val = false;
{
gdjs.groundCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.groundCode.mapOfGDgdjs_46groundCode_46GDwbotObjects1Objects, runtimeScene, true, false);
}if (gdjs.groundCode.condition0IsTrue_0.val) {
gdjs.groundCode.GDhh1Objects1.createFrom(runtimeScene.getObjects("hh1"));
{gdjs.evtTools.camera.centerCamera(runtimeScene, (gdjs.groundCode.GDhh1Objects1.length !== 0 ? gdjs.groundCode.GDhh1Objects1[0] : null), true, "", 0);
}}

}


};gdjs.groundCode.eventsList3 = function(runtimeScene) {

{


gdjs.groundCode.condition0IsTrue_0.val = false;
{
gdjs.groundCode.condition0IsTrue_0.val = gdjs.evtTools.runtimeScene.sceneJustBegins(runtimeScene);
}if (gdjs.groundCode.condition0IsTrue_0.val) {
gdjs.groundCode.GDclothesObjects1.createFrom(runtimeScene.getObjects("clothes"));
{for(var i = 0, len = gdjs.groundCode.GDclothesObjects1.length ;i < len;++i) {
    gdjs.groundCode.GDclothesObjects1[i].hide();
}
}}

}


{



}


{

gdjs.groundCode.GDfsnObjects1.createFrom(runtimeScene.getObjects("fsn"));

gdjs.groundCode.condition0IsTrue_0.val = false;
{
gdjs.groundCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.groundCode.mapOfGDgdjs_46groundCode_46GDfsnObjects1Objects, runtimeScene, true, false);
}if (gdjs.groundCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.groundCode.eventsList1(runtimeScene);} //End of subevents
}

}


{

gdjs.groundCode.GDplayerObjects1.createFrom(runtimeScene.getObjects("player"));

gdjs.groundCode.condition0IsTrue_0.val = false;
gdjs.groundCode.condition1IsTrue_0.val = false;
{
gdjs.groundCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.groundCode.mapOfGDgdjs_46groundCode_46GDplayerObjects1Objects, runtimeScene, true, false);
}if ( gdjs.groundCode.condition0IsTrue_0.val ) {
{
gdjs.groundCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.groundCode.condition1IsTrue_0.val) {
gdjs.groundCode.GDclothesObjects1.createFrom(runtimeScene.getObjects("clothes"));
{for(var i = 0, len = gdjs.groundCode.GDclothesObjects1.length ;i < len;++i) {
    gdjs.groundCode.GDclothesObjects1[i].hide(false);
}
}}

}


{

gdjs.groundCode.GDclothesObjects1.createFrom(runtimeScene.getObjects("clothes"));

gdjs.groundCode.condition0IsTrue_0.val = false;
gdjs.groundCode.condition1IsTrue_0.val = false;
{
gdjs.groundCode.condition0IsTrue_0.val = gdjs.evtTools.input.cursorOnObject(gdjs.groundCode.mapOfGDgdjs_46groundCode_46GDclothesObjects1Objects, runtimeScene, true, false);
}if ( gdjs.groundCode.condition0IsTrue_0.val ) {
{
gdjs.groundCode.condition1IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}}
if (gdjs.groundCode.condition1IsTrue_0.val) {
/* Reuse gdjs.groundCode.GDclothesObjects1 */
{for(var i = 0, len = gdjs.groundCode.GDclothesObjects1.length ;i < len;++i) {
    gdjs.groundCode.GDclothesObjects1[i].hide();
}
}}

}


{


gdjs.groundCode.condition0IsTrue_0.val = false;
{
gdjs.groundCode.condition0IsTrue_0.val = gdjs.evtTools.input.isMouseButtonPressed(runtimeScene, "Left");
}if (gdjs.groundCode.condition0IsTrue_0.val) {

{ //Subevents
gdjs.groundCode.eventsList2(runtimeScene);} //End of subevents
}

}


};

gdjs.groundCode.func = function(runtimeScene) {
runtimeScene.getOnceTriggers().startNewFrame();

gdjs.groundCode.GDfsnObjects1.length = 0;
gdjs.groundCode.GDfsnObjects2.length = 0;
gdjs.groundCode.GDfsnObjects3.length = 0;
gdjs.groundCode.GDplayerObjects1.length = 0;
gdjs.groundCode.GDplayerObjects2.length = 0;
gdjs.groundCode.GDplayerObjects3.length = 0;
gdjs.groundCode.GDDetailsObjects1.length = 0;
gdjs.groundCode.GDDetailsObjects2.length = 0;
gdjs.groundCode.GDDetailsObjects3.length = 0;
gdjs.groundCode.GDrh1Objects1.length = 0;
gdjs.groundCode.GDrh1Objects2.length = 0;
gdjs.groundCode.GDrh1Objects3.length = 0;
gdjs.groundCode.GDclothesObjects1.length = 0;
gdjs.groundCode.GDclothesObjects2.length = 0;
gdjs.groundCode.GDclothesObjects3.length = 0;
gdjs.groundCode.GDsg1Objects1.length = 0;
gdjs.groundCode.GDsg1Objects2.length = 0;
gdjs.groundCode.GDsg1Objects3.length = 0;
gdjs.groundCode.GDhh1Objects1.length = 0;
gdjs.groundCode.GDhh1Objects2.length = 0;
gdjs.groundCode.GDhh1Objects3.length = 0;
gdjs.groundCode.GDwbotObjects1.length = 0;
gdjs.groundCode.GDwbotObjects2.length = 0;
gdjs.groundCode.GDwbotObjects3.length = 0;
gdjs.groundCode.GDwtopObjects1.length = 0;
gdjs.groundCode.GDwtopObjects2.length = 0;
gdjs.groundCode.GDwtopObjects3.length = 0;
gdjs.groundCode.GDwrightObjects1.length = 0;
gdjs.groundCode.GDwrightObjects2.length = 0;
gdjs.groundCode.GDwrightObjects3.length = 0;
gdjs.groundCode.GDwleftObjects1.length = 0;
gdjs.groundCode.GDwleftObjects2.length = 0;
gdjs.groundCode.GDwleftObjects3.length = 0;

gdjs.groundCode.eventsList3(runtimeScene);
return;

}

gdjs['groundCode'] = gdjs.groundCode;
